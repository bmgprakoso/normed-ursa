import {useMemo} from "react";
import {useTable, useGlobalFilter, useSortBy} from "react-table";
import TableLayout from "./TableLayout";

const TableInstance = ({tableData}) => {
    const [columns, data] = useMemo(
        () => {
            const columns = [
                {
                    Header: 'Username',
                    accessor: 'login.username'
                },
                {
                    Header: 'Name',
                    accessor: row => `${row.name.first} ${row.name.last}`
                },
                {
                    Header: 'Email',
                    accessor: 'email'
                },
                {
                    Header: 'Gender',
                    accessor: 'gender'
                },
                {
                    Header: 'Registered Date',
                    accessor: 'registered.date'
                }
            ];
            return [columns, tableData];
        },
        [tableData]
    );

    const tableInstance = useTable(
        {columns, data},
        useGlobalFilter,
        useSortBy
    );

    return (
        <TableLayout {...tableInstance} />
    );
}

export default TableInstance;