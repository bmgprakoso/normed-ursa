import {useContext, useState} from "react";
import {useAsyncDebounce} from 'react-table';
import {GenderFilterContext} from "./hooks/genderFilter";

const TWO_HUNDRED_MS = 200;

const GlobalFilter = ({globalFilter, setGlobalFilter}) => {
    const {gender, setGender} = useContext(GenderFilterContext);

    const [value, setValue] = useState(globalFilter)
    const onChange = useAsyncDebounce(value => {
        setGlobalFilter(value || undefined)
    }, TWO_HUNDRED_MS)

    return (
        <div className="columns">
            <div className="column">
                <div className="field">
                    <label className="label">Search</label>
                    <div className="control">
                        <input
                            className="input"
                            type="text"
                            value={value || ""}
                            onChange={e => {
                                setValue(e.target.value);
                                onChange(e.target.value);
                            }}
                            placeholder={`Search`}
                        />
                    </div>
                </div>
            </div>
            <div className="column is-narrow">
                <div className="field">
                    <label className="label">Gender</label>
                    <div className="control">
                        <div className="select">
                            <select value={gender} onChange={e => setGender(e.target.value)}>
                                <option value="all">All</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div
                className="column"
                style={{
                    display: "flex",
                    alignItems: "flex-end"
                }}
            >
                <div className="control">
                    <button className="button" onClick={() => setGender("all")}>Reset Filter</button>
                </div>
            </div>
        </div>
    )
}

export default GlobalFilter;
