import {createContext, useState} from "react";

export const GenderFilterContext = createContext();

function GenderFilterWrapper({children}) {
    const [gender, setGender] = useState("all");

    const context = {
        gender,
        setGender
    };

    return (
        <GenderFilterContext.Provider value={context}>
            {children}
        </GenderFilterContext.Provider>
    );
}

export default GenderFilterWrapper;