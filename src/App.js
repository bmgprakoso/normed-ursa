import 'bulma/css/bulma.min.css';
import {
    QueryClient,
    QueryClientProvider
} from 'react-query';
import TableQuery from "./TableQuery";
import GenderFilterWrapper from "./hooks/genderFilter";

const client = new QueryClient();

function App() {
    return (
        <QueryClientProvider client={client}>
            <GenderFilterWrapper>
                <TableQuery/>
            </GenderFilterWrapper>
        </QueryClientProvider>
    );
}

export default App;
