import GlobalFilter from "./GlobalFilter";

const TableLayout = ({
                         getTableProps,
                         getTableBodyProps,
                         headerGroups,
                         rows,
                         prepareRow,
                         state: {globalFilter},
                         visibleColumns,
                         preGlobalFilteredRows,
                         setGlobalFilter
                     }) => {
    return (
        <table className="table is-hoverable is-fullwidth" {...getTableProps()}>
            <thead>
            <tr>
                <th colSpan={visibleColumns.length}>
                    <GlobalFilter
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                    />
                </th>
            </tr>
            {headerGroups.map(headerGroup => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map(column => (
                        <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                            {column.render('Header')}
                            <span>
                            {column.isSorted
                                ? column.isSortedDesc
                                    ? ' ⬇️'
                                    : ' ⬆️'
                                : ' ↕️'}
                            </span>
                        </th>
                    ))}
                </tr>
            ))}
            </thead>
            <tbody {...getTableBodyProps()}>
            {rows.map((row) => {
                prepareRow(row)
                return (
                    <tr {...row.getRowProps()}>
                        {row.cells.map(cell => {
                            return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                        })}
                    </tr>
                )
            })}
            </tbody>
        </table>
    );
}

export default TableLayout;