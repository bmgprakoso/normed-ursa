const Pagination = (props) => {
    const {page, onNext, onPrevious, onPageNumber} = props;

    const SIBLING = 1;

    return (
        <nav className="pagination" role="navigation" aria-label="pagination">
            {page > 1 && <a className="pagination-previous" onClick={onPrevious}>Previous</a>}
            <a className="pagination-next" onClick={onNext}>Next page</a>
            <ul className="pagination-list">
                {page - SIBLING > 0 &&
                    <li>
                        <a className="pagination-link" onClick={() => onPageNumber(page - 1)}>{page - 1}</a>
                    </li>}
                <li>
                    <a className="pagination-link is-current">{page}</a>
                </li>
                <li>
                    <a className="pagination-link" onClick={() => onPageNumber(page + 1)}>{page + 1}</a>
                </li>
            </ul>
        </nav>
    );
};

export default Pagination;