import {useQuery} from "react-query";
import {useContext, useEffect, useState} from "react";
import axios from "axios";
import TableInstance from "./TableInstance";
import Pagination from "./Pagination";
import {GenderFilterContext} from "./hooks/genderFilter";

const TableQuery = () => {
    const [page, setPage] = useState(1);
    const [tableData, setTableData] = useState(null);
    const {gender} = useContext(GenderFilterContext);

    const params = {
        page,
        gender,
        results: 10,
    };

    const fetchData = () => axios.get(`https://randomuser.me/api/`, {params});

    const {
        data: apiResponse,
        isLoading
    } = useQuery(['randomUser', page, gender], fetchData, {enabled: !tableData});

    useEffect(() => {
        setTableData(apiResponse?.data);
    }, [apiResponse])

    if (isLoading || !tableData) {
        return <div>Loading...</div>
    }

    return (
        <div className="container pt-3">
            <h1 className="title">Random User</h1>
            <TableInstance tableData={tableData.results}/>
            <Pagination
                page={page}
                onPrevious={() => setPage(page - 1)}
                onNext={() => setPage(page + 1)}
                onPageNumber={page => setPage(page)}
            />
        </div>
    );
}

export default TableQuery;